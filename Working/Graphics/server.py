from functions import *

listValue = []

try:
	Sys.debug("[SYS] Performing inquiry..", "log/server.log")
	nearby_devices = discover_devices(lookup_names = True)
	Sys.debug(f"[SYS] Found {len(nearby_devices)}", "log/server.log")
	
	for name, addr in nearby_devices:
		Sys.debug(f"{addr} - {name}", "log/server.log")

	address = input(str(f"[SYS] Choose one to connect. \n[SHELL]> "))
	#nbaddr = nearby_devices[address]

	sock = BluetoothSocket( RFCOMM )
	sock.connect((address, 1))
			
except KeyboardInterrupt as err:
	Sys.debug(f"[SYS.ERROR] Socket creation error: {err}", "log/server.log")

Sys.debug("[SYS] Connected successfully.", "log/server.log")

try:
	while True:
		try:
			while True:
				dataBluetooth = sock.recv(1024*10)
				Sys.debug(f"[SYS] Receiving information. \n[INFO] {dataBluetooth}.", "log/server.log")

				listValue.append(dataBluetooth.decode("utf-8"))
		
		except KeyboardInterrupt as err:
			Sys.saveInfo(listValue, "recv/.conf")

			conn.close()
			sock.close()
			exit()

except KeyboardInterrupt as err:
	Sys.saveInfo(listValue, "recv/.conf")

	conn.close()
	sock.close()
	exit()