import matplotlib.pyplot as plt

lineList = [line.rstrip('\n') for line in open("recv/20-6.conf")]

listPID = []
listROT = []
listDIS = []
listTIM = []

for value in lineList: 

	if "PID: " in value: listPID.append(value.replace("PID: ", ""))
	elif "ROT: " in value: listROT.append(value.replace("ROT: ", ""))
	elif "DIS: " in value: listDIS.append(value.replace("DIS: ", ""))
	elif "TIM: " in value: listTIM.append(value.replace("TIM: ", ""))


plt.subplot(1, 3, 1)
plt.plot(listPID, listTIM, 'r--')
plt.title("PID error per Time")
plt.ylabel("PID error")
plt.xlabel("Time")


plt.subplot(1, 3, 2)
plt.plot(listROT, listTIM, 'r--')
plt.title("Rotation per Time")
plt.ylabel("Rotation")
plt.xlabel("Time")


plt.subplot(1, 3, 3)
plt.plot(listDIS, listTIM, 'r--')
plt.title("Distance per Time")
plt.ylabel("Distance")
plt.xlabel("Time")


plt.show()