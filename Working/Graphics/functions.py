import datetime, os
from bluetooth import *

time = datetime.datetime.now()

class Sys(object):
	"""docstring for Sys"""
	def __init__(self, arg, file):
		super(Sys, self).__init__()
		self.arg = arg
		self.file = file
		
	def debug(arg, file):
		print(arg)

		try:
			logDir = ""
			tempVar = ""
			isFile = False
			
			for i in file:
				if i == "/": isFile = True
				elif isFile: tempVar += i
				else: logDir += i

			os.mkdir(logDir)

		except FileExistsError: pass
		
		try: open(file.replace("/", f"/{time.day}.{time.month}-"), "r")
		
		except FileNotFoundError:

			createFile = open(file.replace("/", f"/{time.day}.{time.month}-"), "w")
			createFile.close()

		with open(file.replace("/", f"/{time.day}.{time.month}-"), "r") as infile:
			readFile = infile.read()
			infile.close()

			with open(file.replace("/", f"/{time.day}.{time.month}-"), "wb") as outfile:
				outfile.write(bytes(readFile+f"{time.hour}:{time.minute}:{time.second} - {arg}\n", "utf8"))
				outfile.close()

		#print("FILE: %s"%file)
		#print("ARG: %s"%arg)


	def saveInfo(arg, file):
		try:
			fileDir = ""
			tempVar = ""
			isFile = False
				
			for i in file:
				if i == "/": isFile = True
				elif isFile: tempVar += i
				else: fileDir += i

			os.mkdir(fileDir)
			
		except FileExistsError:
			pass
			
		'''To the analyze scritp'''
		try: open(file.replace("/", f"/{time.day}-{time.month}"), "r")
			 
		except FileNotFoundError:
			createFile = open(file.replace("/", f"/{time.day}-{time.month}"), "w")
			createFile.close()

		with open(file.replace("/", f"/{time.day}-{time.month}"), "r") as infile:
			inFile = infile.read()
			infile.close()

			with open(file.replace("/", f"/{time.day}-{time.month}"), "w") as outfile: 
				for i in range(len(arg)):
					outfile.write(str(arg[i]))
				outfile.close()

		'''To the machine learning'''
		try: open("recv/learn.csv", "r")

		except FileNotFoundError:
			createFile = open("recv/learn.csv", "w")
			createFile.close()
		
		with open("recv/learn.csv", "r") as infile:
			inFile = infile.read()
			infile.close()


			with open("recv/learn.csv", "w") as outfile:

				if inFile == "":
					print("There is anything")
					outfile.write("PID, Rotation, Distance, Time\n")

					for i in range(len(arg)):
						outfile.write(str(arg[i]))

				else: 
					outfile.write(inFile)
				
					for i in range(len(arg)): 
						outfile.write(str(arg[i]))

				outfile.close()

			with open("recv/learn.csv", "r") as infile:
				inFile = infile.read()
				infile.close()

				tempVar = inFile.replace("PID: ", "")
				tempVar = tempVar.replace("ROT: ", ",")
				tempVar = tempVar.replace("DIS: ", ",")
				tempVar = tempVar.replace("TIM: ", ",")
				tempVar = tempVar.replace("\nPID: ", "\n")
				tempVar = tempVar.replace("\nROT: ", ",")
				tempVar = tempVar.replace("\nDIS: ", ",")
				tempVar = tempVar.replace("\nTIM: ", ",")
				tempVar = tempVar.replace("\n,", ",")

				with open("recv/learn.csv", "w") as outfile:
					outfile.write(tempVar)
					outfile.close()