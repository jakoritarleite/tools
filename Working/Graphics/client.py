import socket # for socket 
import sys  
import random
from time import sleep
  
try: 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    print("Socket successfully created")
except socket.error as err: 
    print("socket creation failed with error %s" %(err)) 
  
# default port for socket 
port = 80
  
try: 
    host_ip = socket.gethostbyname('localhost') 
except socket.gaierror: 
  
    # this means could not resolve the host 
    print("there was an error resolving the host")
    sys.exit() 
  
# connecting to the server 
s.connect((host_ip, port)) 


while True:
	
    randomValuesPID = random.randint(1,101)
    randomValuesROT = random.randint(102, 202)
    randomValuesDIS = random.randint(203, 303)
    randomValuesTIM = random.randint(304, 404)
    '''
	#print(bytes(str(randomValues), 'utf8'))

	s.send(bytes(str(randomValues) + "\n", 'utf8'))

	sleep(1)
    '''

    s.send(bytes("PID: "+str(randomValuesPID)+"\n", 'utf8'))
    s.send(bytes("ROT: "+str(randomValuesROT)+"\n", 'utf8'))
    s.send(bytes("DIS: "+str(randomValuesDIS)+"\n", 'utf8'))
    s.send(bytes("TIM: "+str(randomValuesTIM)+"\n", 'utf8'))

    sleep(0.5)


