**LINKS**


`Encoder:`
*   [GitHub Lib](https://github.com/mathertel/RotaryEncoder/)  
*   [Arduino Libs](https://playground.arduino.cc/Main/RotaryEncoders/)



`Schematic Arduino/H-Bridge/Motors/ArraySensors/SideSensors:`
*	[Google Drive](https://docs.google.com/spreadsheets/d/1YRJYs6rg-udYGMo4arT9UJiDMNqzzqJKhxyzseiR4Gw/edit#gid=0)


`Arduino:`
*   [Pinout](https://www.theengineeringprojects.com/wp-content/uploads/2018/06/introduction-to-arduino-nano-13-1.png)
*   [Operators](https://pt.wikipedia.org/wiki/Operadores_em_C_e_C%2B%2B)