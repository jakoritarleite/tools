#include <stdint.h>
#include <math.h>
#include "ALFR.h"

/*		Defining Variables		*/

bool sensorDigital[sensorCount] = {false, false, false, true, true, false, false, false};
int error;

int differentialValue[sensorCount];
int calibrateValue[sensorCount];

//Encoders
long oldPositionA = NULL;
boolean doBoost = false;

//Boost
boolean doBoost = false; //If it is true, the car is do a boost based on the encoder's value
int startBoostTime[2] = {};
int endBoostTime[2] = {};

//Bluetooth
boolean useBluetooth = false;

//Motors
bool keys[4] = {HIGH, LOW, HIGH, LOW};

bool motorsKey(String directionA, String directionB) {
	if (directionA == "Forward") {keys[0] = HIGH; keys[1] = LOW;}
	else if (directionA == "Back") {keys[0] = LOW; keys[1] = HIGH;}
	if (directionB == "Forward") {keys[2] = HIGH; keys[3] = LOW;}
	else if (directionB == "Back") {keys[2] = LOW; keys[3] = HIGH;}
} 

void motorsToWork(int A, int B, int value1, int value2, int value3, int value4) {
  	digitalWrite(motorAs1, value1);
	digitalWrite(motorAs2, value2);
	digitalWrite(motorBs1, value3);
	digitalWrite(motorBs2, value4);

	analogWrite(pwmA, A);
	analogWrite(pwmB, B);
}

void encodersToWork() {
	long newPositionA = myEncA.read();

	if (doBluetooth) {
      Serial.print("Encoder tick: ");
      Serial.print(newPositionA);
      Serial.println();
	}
  //If you want to know which is the encoder value, just define useBluetooth to true and put bluetooth RX and TX on the arduino's RX/TX. 
	
  if ((newPositionA - oldPositionA) <= -61500) {
    while (true) {motorsToWork(0, 0, HIGH, HIGH, HIGH, HIGH);}
  }
}