#include <Encoder.h>
#include <QTRSensors.h>
#include "Untitled.h"

//Motors pins
#define motorAs1
#define motorAs2

#define motorBs1 
#define motorBs2 

#define pwmA 
#define pwmB 

//Encoders pins
#define encoderA1 
#define encoderA2 

#define encoderB1
#define encoderB2

//Bluetooth pins
#define pinRX 
#define pinTX 

//Other pin
#define CALIBRATE_PIN

//Defining functions
QTRSensors qtr;
Encoder myEncA(encoderA1, encoderA2);
Encoder myEncB(encoderB1, encoderB2);

/*		Defining Variables		*/
/*Sensors*/
const int8_t sensorCount = 0;
uint16_t sensorValue[sensorCount];

void setup() {
	Serial.begin(9600);

	qtr.setTypeAnalog();
	qtr.setSensorPins((const uint8_t[]){}, sensorCount);

	analogWrite(CALIBRATE_PIN, 255);
	for (int i = 0; i < sensorCount; i++) {qtr.calibrate();}
	analogWrite(CALIBRATE_PIN, 0);

 	firstPositionA = myEncA.read();
 	firstPositionB = myEncB.read();

 	delay(5000);
}

void loop() {
	uint16_t position = sensorConfigure();

	float pidValue[3] = PIDbySensor(position);
	float adrcValue[3] = ADRC(pidValue[1], pidValue[2]);

	Controller(pidValue[1], pidValue[2], adrcValue[1], adrcValue[2]);
}

//64 + 116 + 67 = 247