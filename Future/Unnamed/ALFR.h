#include <Arduino.h>
#include <stdint.h>

#define _USE_MATH_DEFINES
#include <math.h>

/* Defining important varibles */

/*PID Variables*/
float Kp = 0;
float Ki = 0;
float Kd = 0;

float error, P, I, D, valuePID = 0;
float previousError, previousI = 0;

/*ADRC Variables*/
float rightRPM, leftRPM; 
float rightAngle, leftAngle; //tetaR, tetaL
float angularPosition; //alpha
float radius = 2.25; //r
float diammeter = 12; //L
float differenceSpeed; //e
float position; //p
float midpointSensorToRobot = 15; //a (distance from the midpoint of the sensor to robot's midpoint)
float lastPosition = 0;

/* Trial matriz eg

	| 0   1   |			Access 100:		Access 200:
	| 100 200 | 2x2		matriz[0][0] 	matriz[1][0]
+
	| 150 250 | 2x3		Access 150:		Access 250:
						matriz[0][1]  	matriz[1][1]


						{ {100, 150}, {200, 250} }
*/

uint16_t sensorConfigurate() {
	uint16_t p = qtr.readLineWhite(sensorValues);

	for (uint8_t i = 0; i < sensorCount; i++) {
		differentialValue[i] = qtr.calibrationOn.minimum[i];
		calibrateValue[i] = sensorValues[i] - differentialValue[i];
		
		uint16_t finalValue += calibrateValue[i];
	}

	return(finalValue);
}

float PIDbySensor(int pos) {
	int err = (pos - 3500) / 7000;

	P = err;
	I ;= I + err;
	D = err - previousError;

	valuePID = (Kp * P) + (Ki * I) + (Kd * D); //motorSpeed
	previousError = err;

	float speedR = baseSpeed + (baseSpeed * (valuePID * -1));
	float speedL = baseSpeed + (baseSpeed * valuePID);

	if (speedR < 0) { speedR *= -1; motorsKey("Back", "Forward"); }
	if (speedR > 255) { speedR = 255; }
	if (speedL < 0) { speedR *= -1; motorsKey("Forward", "Back"); }

	else if (speedR > 0 && speedL > 0) { motorsKey("Forward", "Forward"); }

	return(valuePID, speedR, speedL);
}

float PIDbyEncoder(positionA, positionB) {
	return;
}

float ADRC(int speedA, int speedB) {
	rightRPM = (M_PI / 30) * (speedA);
	leftRPM = (M_PI / 30) * (speedB);

	rightAngle = ((2 * M_PI) * rightRPM) / 60;
	leftAngle = ((2 * M_PI) * leftRPM) / 60;

	angularPosition = ((rightAngle - leftAngle) * radius) / diammeter;

	differenceSpeed = rightRPM - leftRPM;

	position = midpointSensorToRobot * tan( (M_PI / 30) * ( radius / diammeter ) * ( (differenceSpeed ** 2) / 2) );

 	//I don't know how it's gonna work

	float valueADRC = (Kp * position) + (Ki + I) + (Kd * (position - lastPosition));
	lastPosition = position;

	float speedRi = baseSpeed + (valueADRC);
	float speedLi = baseSpeed - (valueADRC);

	//After, do the medium between ADRC' speeds and PID' speed and then add to speeds. //This function I will need the robot working to test and verify. 

	return(position, speedRi, speedLi);
}

void Controller(speedAPID, speedBPID, speedAADRC, speedBADRC) {
	float speedAf = (speedAPID + speedAADRC) / 2;
	float speedBf = (speedBPID + speedBADRC) / 2;

	motorsToWork(speedAf, speedBf, motorsKey[0], motorsKey[1], motorsKey[2], motorsKey[3]);
	encodersToWork();
}

float AT() {
	//Studying how to do autotuning
}