try:
	import argparse
	from math import tan
	from sys import exit
	from time import sleep
	from os import mkdir

except ModuleNotFoundError:
	with open("requirements.txt", "w") as outFile:
		outFile.write("argparse\nmath\nsys\ntime\nmatplotlib\n")
		outFile.close()

	print("[!] Install the packages in requirements.txt to run this script.")
	print("[*] Aborting..")

	exit()

parser = argparse.ArgumentParser(usage="python sim.py [-h] [-i integral] [-s speed] [-v variation] [-d debug]" ,description="Easy script to simulate PID controller.")
parser.add_argument("-i", dest="integral", action="store", default="false", type=str, help="If you want to use integral on PID equation.")
parser.add_argument("-s", dest="speed", action="store", default=100, type=int, help="The initial speed.")
parser.add_argument("-v", dest="variation", action="store", default=0, type=int, help="If you want speed variation (Pattern speed variation is 0)")
parser.add_argument("-d", dest="debug", action="store", default="true", type=str, help="If you don't want to debug use [-d false]")

result = parser.parse_args()

M_PI = 3.141592654

useIntegral = False
changeSpeed = False

baseSpeed = result.speed
maxSpeed = 255

if result.integral == "true":
	useIntegral = True

if result.variation != 0:
	changeSpeed = True

initVariation = 0

Kp = 1
Ki = 1
Kd = 1

P, I, D = 0, 0, 0

positionSensor = 0
previousError = 0

times = 0
stopLoop = False

sensorMiddleToRobotMiddle = 10
radius = 1.15
diammeter = 9

def writeIn(stateOne, stateTwo, stateThree, error, control):
	try: open("tests/{0}/test-{1}.txt".format(control, baseSpeed), "r")
	except FileNotFoundError:
		try: 
			mkdir("tests")
			mkdir("tests/PID")
			mkdir("tests/ADRC")

		except FileExistsError: pass
		create = open("tests/{0}/test-{1}.txt".format(control, baseSpeed), "w")
		create.write("######### {0} #########".format(baseSpeed))
		create.close()

	with open("tests/{0}/test-{1}.txt".format(control, baseSpeed), "r") as inFile:
		fileIn = inFile.read()
		inFile.close()

		with open("tests/{0}/test-{1}.txt".format(control, baseSpeed), "w") as outFile:
			if control == "PID": fileOut = fileIn + "\n\nError: " + str(error) + "\nKp: " + str(Kp) + "\nKi: " + str(Ki) + "\nKd: " + str(Kd)
			elif control == "ADRC": fileOut = fileIn + "\n\nPosition: " + str(error) + "\nspeedR: " + str(stateOne) + "\nspeedL: " + str(stateTwo)
			outFile.write(fileOut)
			outFile.close()


class ControlSimulate(object):
	"""docstring for ControlSimulate"""
	def __init__(self, arg):
		super(ControlSimulate, self).__init__()
		self.arg = arg
		
	def PID():
		global positionSensor, previousError, Kp, Ki, Kd, P, I, D, initVariation, baseSpeed, maxSpeed, times
		
		if times == 5:
			Kp, Ki, Kd = 1, 1, 1
			previousError = 0
			P, I, D = 0, 0, 0
			positionSensor = 0
			baseSpeed += result.variation
			times = 0

		if positionSensor > 7000:
			times += 1
			positionSensor = 0
			P, I, D = 0, 0, 0
			Kp = Kp + Kp * 0.1
			Ki = Ki + Ki * 0.001
			Kd = Kd + Kd * 0.01
		
		if (baseSpeed + initVariation) > 255:
			print("[*] All done.")
			exit()

		right, left = "Forward", "Forward"

		error = (positionSensor - 3500) / 7000

		P = error
		if useIntegral: I = I + error
		D = error - previousError
		previousError = error

		valuePID = (Kp * P) + (Ki * I) + (Kd * D)

		speedR = baseSpeed + (baseSpeed * (valuePID * -1 ))
		speedL = baseSpeed + (baseSpeed * valuePID)

		if speedR < 0:
			speedR *= -1
			right = "Back"

		if speedL < 0:
			speedL *= -1
			left = "Back"

		if speedR > maxSpeed:
			speedR = maxSpeed

		if speedL > maxSpeed:
			speedL = maxSpeed

		if result.debug == "true":
			print()
			print("Error: {}".format(error))
			print("Position: {}".format(positionSensor))
			print("PID: {}".format(valuePID))
			print("speedR: {} > {}".format(speedR, right))
			print("speedL: {} > {}".format(speedL, left))
			print("Times: {}".format(times))
			sleep(0.05)

		writeIn(Kp, Ki, Kd, error, "PID")

		positionSensor += 500

		return(speedR, speedL)

	def ADRC(speedA, speedB, bell):
		global sensorMiddleToRobotMiddle, radius, diammeter

		rightRPM = (M_PI / 30) * (speedA)
		leftRPM = (M_PI / 30) * (speedB)

		differenceSpeed = rightRPM - leftRPM #rightRPM - leftRPM #speedA - speedB 

		positionByADRC = sensorMiddleToRobotMiddle * tan( (M_PI / 30) * (radius / diammeter) * ((differenceSpeed ** 2) / 2) )  

		right, left = "Forward", "Forward"

		newSpeedR = (speedA) + (rightRPM * (positionByADRC * -1))
		newSpeedL = (speedB) + (leftRPM * positionByADRC)

		if newSpeedR < 0:
			newSpeedR *= -1
			right = "Back"

		if newSpeedL < 0:
			newSpeedL *= -1
			left = "Back"

		if newSpeedR > maxSpeed:
			newSpeedR = maxSpeed

		if newSpeedL > maxSpeed:
			newSpeedL = maxSpeed


		if result.debug == "true":
			#print("ADRC: {}".format(positionByADRC))
			pass

		if bell == 2:
			print()
			#print(f"[RPM]speedR: {rightRPM}")
			#print(f"[RPM]speedL: {leftRPM}")		
			print(f"speedR: {speedA}")
			print(f"speedL: {speedB}")
			print("ADRC: {}".format(positionByADRC))
			print(f"newSpeedR: {newSpeedR} > {right}")
			print(f"newSpeedL: {newSpeedL} > {left}")
			sleep(0.05)

		writeIn(speedA, speedB, Kd, positionByADRC, "ADRC")
		
		##Correct ADRC

		return(newSpeedR, newSpeedL)


		def ILC(PID1, PID2, ADRC1, ADRC2):
			mediumPID = (PID1 + PID2) / 2
			mediumADRC = (ADRC1 + ADRC2) / 2

			print(mediumPID)


while True:
	try: 
		PID = ControlSimulate.PID()
		ADRC = ControlSimulate.ADRC(PID[0], PID[1], 2)
		#ControlSimulate.ADRC(PID[0] - ADRC, PID[1] + ADRC, 2)
		ControlSimulate.ILC(PID[0], PID[1], ADRC[0], ADRC[1])


	except KeyboardInterrupt: 
		print("[*] User requested to abort.")
		exit()

#Capture All errors during the lap
#Save into an array
#And use this script to study all the errors with different speeds and PID values.


#Write this code in C++
