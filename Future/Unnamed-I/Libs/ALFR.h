#include <stdint.h>
#include "Arduino.h"

#define _USE_MATH_DEFINES
#include <math.h>

//PID Variables
float Kp = 0;
float Ki = 0;
float Kd = 0;

float error, P, I, D, valuePID = 0;
float previousError, previousI = 0;

//ADRC Variables
const float radius; //Radius of the wheel
const float diammeter; //Diammeter of the body car
const float midpointSensorToRobot; //a (distance from the midpoint of the sensor to robot's midpoint)

//ILC Varialbles;
float mediumErrorsPID*;
float mediumErrorsADRC*;
int timesRunned = 0;

uint16_t sensorConfigurate() {
	float differentialValue[sensorCount] = {0};
	float calibrateValue[sensorCount] = {0};

	uint16_t p = qtr.readLineWhite(sensorValue);

	for (uint8_t i = 0; i < sensorCount; ++i) {
		//differentialValue[i] = qtr.calibrationOn.minimum[i];
		calibrateValue[i] = sensorValue[i] - qtr.calibrationOn.minimum[i]; //differentialValue[i];
	
		uint16_t finalValue += calibrateValue[i];
	}

	return(finalValue);
}

float PIDbySensor(uint16_t position) {
	int error = (position - 3500) / 7000;

	P = error;
	I = I + error;
	D = error - previousError;
	previousError = error;

	valuePID = (Kp * P) + (Ki * I) + (Kd * D);

	float speedR = baseSpeed + (baseSpeed * (valuePID * -1));
	float speedL = baseSpeed + (baseSpeed * valuePID);

	if (speedR < 0) {
		speedR *= -1;
		setMotorsKey(0, 1);
	} if (speedR > maxSpeed) { speedR = maxSpeed; }

	if (speedL < 0) {
		speedL *= -1;
		setMotorsKey(1, 0);
	} if (speedL > maxSpeed) { speedL = maxSpeed; }

	return(valuePID, speedR, speedL);
}

float ADRC(float speedA, float speedB) {
	float rightRPM = (M_PI / 30) * (speedA);
	float leftRPM = (M_PI / 30) * (speedB);

	float differenceSpeed = rightRPM - leftRPM;

	float positionByADRC = midpointSensorToRobot * tan( (M_PI / 30) * (radius / diammeter) * ((differenceSpeed ** 2) / 2) );

	float speedR = (speedA) + (rightRPM * (positionByADRC * -1));
	float speedL = (speedB) + (leftRPM * positionByADRC);

	if (speedR < 0) {
		speedR *= -1;
		setMotorsKey(0, 1);
	} if (speedR > maxSpeed) { speedR = maxSpeed; }

	if (speedL < 0) {
		speedL *= -1;
		setMotorsKey(1, 0);
	} if (speedL > maxSpeed) { speedL = maxSpeed; }

	return(positionByADRC, speedR, speedL);
}

float ILC(float positionByPID, float positionByADRC){
	mediumErrorsPID[timesRunned] = positionByPID; 
	mediumErrorsADRC[timesRunned] = positionByADRC;

	timesRunned++;

	float _mediumP = 0;
	float _mediumA = 0;
	for(int i = 0; i < timesRunned; i++) {
		_mediumP += mediumErrorsPID[timesRunned];
		_mediumA += mediumErrorsADRC[timesRunned];
	}

	_mediumP /= timesRunned;
	_mediumA /= timesRunned;

	return(_mediumP, _mediumA);
}

void Controler(float _ilc, float _pid, float _adrc) {
	/*
	float mediumPID = (_pid[1] + _pid[2]) / 2;
	float mediumADRC = (_adrc[1] + _adrc[2]) / 2;
	*/
	
	//if pidMedium < adrcMedium (what is nearly 0)
	if (_ilc[0] < _ilc[1]) {
		motorsToWork(_pid[1], _pid[2]);
	} else if (ilc[2] < _ilc[1]) {
		motorsToWork(_adrc[1], _adrc[2]);
	}
}