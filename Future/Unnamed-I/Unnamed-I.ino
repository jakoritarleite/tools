#include <Encoder.h>
#include <QTRSensors.h>
#include "Libs/Untitled.h"
#include "Libs/ALFR.h"

//Motors pins
#define motorAs1
#define motorAs2 

#define motorBs1
#define motorBs2

#define pwmA
#define pwmB

//Encoders pins
#define encoderA1
#define encoderA2

#define encoderB1
#define encoderB2 

//Bluetooth pins
#define pinRX
#define pinTX

//Util pins
#define CALIBRATE_PIN //This is the pin for a led

//Defining functions
QTRSensors qtr;
Encoder myEncA(encoderA1, encoderA2);
Encoder myEncB(encoderB1, encoderB2);

/*Defining variables*/
//Sensors
const int8_t sensorCount = 0;
uint16_t sensorValue[sensorCount];

//Encoders Work
const long int encoderLastPosition; //When you need to stop the car

//Motors
short int baseSpeed = 100;
short int maxSpeed = 255;

void setup() {
	Serial.begin(9600);

	qtr.setTypeAnalog();
	qtr.setSensorPins((const uint8_t[]){}, sensorCount);

	analogWrite(CALIBRATE_PIN, 255);
	for(int i = 0; i < sensorCount; ++i) {qtr.calibrate();}
	analogWrite(CALIBRATE_PIN, 0);

	firstPositionA = myEncA.read();
	firstPositionB = myEncB.read();

	delay(5000);
}

void loop() {
	uint16_t position = sensorConfigure();

	float pidValue[3] = PIDbySensor(position);
	float adrcValue[3] = ADRC(pidValue[1], pidValue[2]);
 
	float ilc = ILC(pidValue[0], adrcValue[0]);

	Controller(ilc, pidValue, adrcValue);

	/*
	pidValue[0] > is the error gen by PID
	pidValue[1] > is the right motor speed
	pidValue[2] > is the left motor speed

	adrcValue[0] > is the error gen by ADRC
	adrcValue[1] > is the right motor speed
	adrcValue[2] > is the left motor speed
	*/	

}